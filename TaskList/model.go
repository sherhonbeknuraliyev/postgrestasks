package main

import (
	"fmt"
)

type Task struct {
	ID, Name, Status, Priority,CreatedBy string
	CreatedAt, DueDate string
}

func AddTask(id,newTask Task) (Task, error) {
	msg := fmt.Sprintf(`INSERT INTO tasks (name, status, priority, createdby,createdat,duedate) VALUES ($1,$2,$3,$4,$5,$6);`)
	_, err := db.Query(msg, newTask.Name, newTask.Status, newTask.Priority, newTask.CreatedBy, newTask.CreatedAt, newTask.DueDate)
	if err != nil {
		return Task{}, err
	}

	return GetByName(newTask.Name), err
}
func GetAll()[]Task  {
	rows, err:=db.Query("SELECT * FROM tasks;")
	if err != nil{
		panic(err)
	}
	var tasks[] Task
	for rows.Next(){
		var ID, Name,Status,Priority, CreatedBy string
		var CreatedAt, DueAt string
		err := rows.Scan(&ID, &Name,&Status,&Priority, &CreatedBy,&CreatedAt,&DueAt)
		if err != nil{
			panic(err)
		}
		contact := Task{ID: ID,Name: Name,Status: Status,Priority: Priority,CreatedBy: CreatedBy,CreatedAt: CreatedAt,DueDate: DueAt}
		tasks = append(tasks,contact)
	}
	return tasks
}
func GetByName(name string) Task {
	msg := fmt.Sprintf(`SELECT * FROM tasks WHERE name=$1;`)
	rows , err := db.Query(msg,name)

	var ID, Name,Status,Priority, CreatedBy string
	var CreatedAt, DueAt string
	err = rows.Scan(&ID, &Name, &Status, &Priority, &CreatedBy, &CreatedAt, &DueAt)
	if err != nil{
		panic(err)
	}

	return Task{ID,Name,Status,Priority,CreatedBy,CreatedAt,DueAt}
}
func GetById(id string) Task {
	msg := fmt.Sprintf(`SELECT * FROM tasks WHERE id=$1;`)
	rows , err := db.Query(msg,id)
	var ID, Name,Status,Priority, CreatedBy string
	var CreatedAt, DueAt string
	err = rows.Scan(&ID, &Name, &Status, &Priority, &CreatedBy, &CreatedAt, &DueAt)
	if err != nil{
		panic(err)
	}
	return Task{ID,Name,Status,Priority,CreatedBy,CreatedAt,DueAt}
}
func UpdateTask(ID string,newTask Task) (Task,error) {
	msg := fmt.Sprintf(`UPDATE tasks SET name=$1, status=$2, priority=$3,createdby=$4,createdat=$5,duedate=$6 WHERE id=$7;`)
	_, err := db.Query(msg, newTask.Name, newTask.Status,newTask.Priority, newTask.CreatedBy, newTask.CreatedAt, newTask.DueDate, ID)
	if err != nil {
		return Task{}, err
	}
	return GetById(ID), nil
}
