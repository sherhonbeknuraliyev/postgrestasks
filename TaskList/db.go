package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
)

const (
	host = "localhost"
	port = 5432
	user = "postgres"
	password = "root"
	dbname = "golangtestdb"
)

func connectDb() (*sql.DB, error) {
	pInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",host,port, user,password,dbname)
	db_,err := sql.Open("postgres",pInfo)
	if err != nil{
		fmt.Println(err)
		return &sql.DB{}, err
	}
	err = db_.Ping()
	if err != nil {
		fmt.Println(err)
		return &sql.DB{}, err
	}
	fmt.Println("Successfully connect to db ")
	return db_, nil
}
