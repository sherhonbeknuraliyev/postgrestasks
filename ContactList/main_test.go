package main

import (
	"fmt"
	"log"
	"testing"
)

const (
	host = "localhost"
	port = 5432
	user = "postgres"
	password = "root"
	dbname = "for_test"
)
var dbData = DbData{host: host, user: user,password: password,dbname: dbname, port: port}
var db,_ = connectDb(dbData)
var cm = ContactManger{
	db: db,
}


func TestCreateTable(t *testing.T)  {
	err := cm.deleteContactTable()
	if err != nil {
		fmt.Println(err)
	}
	err = cm.createContactTable()
	if err != nil {
		log.Fatal(err)
	}
}


func TestCreateContact(t *testing.T)  {
	cont := Contact{Email: "asd@gmail.com",Phone: "+4234254325", FirstName: "Bob", LastName: "John",Position: "Dev"}
	var err error
	contact, err := cm.save(&cont)
	if err != nil {
		log.Fatalln("Create Contact:",err)
	}
	if contact == nil{
		log.Fatal("didn't create contact")
	}

}
func TestDeleteContact(t *testing.T)  {
	var contact1 = Contact{
		Email: "delete_contact@gmail.com",
		Phone: "+123",
		FirstName: "Test2",
		LastName: "Test2",
		Position: "Tester2",
	}
	_, err := cm.save(&contact1)
	if err != nil {
		log.Fatal(err)
	}


}

func TestGetByPhone(t *testing.T)  {
	var contact1 = Contact{
		Email: "test_get_by_phone@gmail.com",
		Phone: "+1234",
		FirstName: "Test2",
		LastName: "Test2",
		Position: "Tester2",
	}
	_, err := cm.save(&contact1)
	if err != nil {
		log.Fatalln("CreateContact:",err)
	}
	_,err = cm.getByPhone("+1234")
	if err != nil{
		log.Fatalln("GetByPhone:",err)
	}


}
func TestGetByID(t *testing.T)  {
	var contact1 = Contact{
		Email: "test_get_by_id@gmail.com",
		Phone: "+12345",
		FirstName: "Test2",
		LastName: "Test2",
		Position: "Tester2",
	}
	_, err := cm.save(&contact1)
	if err != nil {
		log.Fatalln("CreateContact: " , err)
	}
	_, err = cm.getByID(1)
	if err != nil{
		log.Fatal("GetById:", err)
	}
}

func TestUpdateContact(t *testing.T)  {
	var contact1 = Contact{
		Email: "test_update_contact@gmail.com",
		Phone: "+123456",
		FirstName: "Test2",
		LastName: "Test2",
		Position: "Tester2",
	}
	_, err := cm.save(&contact1)
	if err != nil {
		log.Fatalln("CreateContact: " , err)
	}
	contact,err := cm.getByPhone("+123456")
	if err != nil{
		log.Fatalln("GetByPhone:",err)
	}
	newContact := Contact{Email: "update@gmail.com",Phone: "+321", FirstName: "Bob", LastName: "John",Position: "Tester"}
	updatedContact, err := cm.update(newContact, contact.ID)
	if err != nil {
		log.Fatal("Update contact:",err)
	}

	if updatedContact.Position != "Tester"{
		log.Fatalln("Update contact:","Not updated")
	}
}
func TestDeleteTable(t *testing.T)  {
	err := cm.deleteContactTable()
	if err != nil {
		log.Fatal(err)
	}
}
func createTable()  {
	err := cm.createContactTable()
	if err != nil {
		panic(err)
	}

}
func dropTable()  {
	err := cm.deleteContactTable()
	if err != nil {
		panic(err)
	}
}
