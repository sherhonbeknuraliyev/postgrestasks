package main

import (
	"database/sql"
	"fmt"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)


type DbData struct {
	host, user, password, dbname string
	port int
}

func connectDb(data DbData) (*sql.DB, error) {
	pInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",data.host,data.port, data.user,data.password,data.dbname)
	db,err := sql.Open("postgres",pInfo)
	if err != nil{
		return &sql.DB{}, err
	}
	err = db.Ping()
	if err != nil {
		return &sql.DB{}, err
	}
	fmt.Println("Successfully connect to db ")
	return db, nil
}

