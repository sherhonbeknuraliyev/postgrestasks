package main

import (
	"database/sql"
	"errors"
	"fmt"
)

type Contact struct {
	ID int
	FirstName, LastName, Phone, Email, Position string
}
type ContactManger struct {
	db *sql.DB

}
func (contactManager *ContactManger)save(contact *Contact)(*Contact,error) {

	data := fmt.Sprintf(`INSERT INTO contacts (firstname,lastname, phone, email, position) VALUES ($1,$2,$3,$4,$5) RETURNING ID;`)

	rows, err := contactManager.db.Query(data,contact.FirstName, contact.LastName, contact.Phone,contact.Email, contact.Position)
	if err != nil{
		return nil, err
	}
	var ID int
	if rows.Next(){
		err := rows.Scan(&ID)
		if err != nil {
			return nil, err
		}
	}
	contact.ID = ID
	return contact, err

}
func (contactManager ContactManger)update(newContact Contact,ID int) (*Contact,error)  {
	data := fmt.Sprintf(`UPDATE contacts SET firstname=$1,lastname=$2, phone=$3, email=$4, position=$5 WHERE id=$6 RETURNING ID;`)
	rows, err := contactManager.db.Query(data, newContact.FirstName, newContact.LastName, newContact.Phone, newContact.Email, newContact.Position, ID)
	if err != nil {
		return nil, err
	}
	var id int
	if rows.Next(){
		err := rows.Scan(&id)
		if err != nil {
			return nil, err
		}

	}
	newContact.ID = id

	return &newContact, nil

}
func (contactManager ContactManger)getByPhone(phone string) (*Contact, error ){
	msg := fmt.Sprintf("SELECT * FROM contacts WHERE phone='%v';",phone)
	rows, err:=contactManager.db.Query(msg)
	if err != nil{
		return nil, err
	}

	if rows.Next() {
		var ID int
		var  FirstName,LastName,Phone, Email,Position string
		err := rows.Scan(&ID, &FirstName,&LastName,&Phone, &Email,&Position)
		if err != nil{
			return nil, err
		}
		contact := Contact{ID: ID,FirstName: FirstName,Email:Email,LastName: LastName,Phone: Phone,Position: Position}
		return &contact, nil
	}
	return nil, errors.New("not found contact")

}
func (contactManager ContactManger)getByID(id int) (*Contact, error ){
	msg := fmt.Sprintf("SELECT * FROM contacts WHERE id='%d';", id)
	rows, err:=contactManager.db.Query(msg)
	if err != nil{
		return nil, err
	}

	if rows.Next() {
		var ID int
		var  FirstName,LastName,Phone, Email,Position string
		err := rows.Scan(&ID, &FirstName,&LastName,&Phone, &Email,&Position)
		if err != nil{
			panic(err)
		}
		contact := Contact{ID: ID,FirstName: FirstName,LastName: LastName,Phone: Phone,Position: Position}
		return &contact,nil
	}
	return nil, errors.New("not found")

}
func (contactManager ContactManger)getAll()([]Contact, error ) {
	rows, err:=contactManager.db.Query("SELECT * FROM contacts;")
	if err != nil{
		return nil,err
	}
	var contacts[] Contact
	for rows.Next(){
		var ID int
		var  FirstName,LastName,Phone, Email,Position string
		err := rows.Scan(&ID, &FirstName,&LastName,&Phone, &Email,&Position)
		if err != nil{
			panic(err)
		}
		contact := Contact{ID: ID,FirstName: FirstName,LastName: LastName,Phone: Phone,Position: Position}
		contacts = append(contacts,contact)
	}
	return contacts, nil
}
func (contactManager ContactManger)deleteContactByPhone(phone string)( error) {
	msg := fmt.Sprintf(`DELETE FROM contacts WHERE phone=$1 RETURNING ID`)
	rows, err := contactManager.db.Query(msg, phone)
	if err != nil {
		return  err
	}
	var id int
	if rows.Next(){
		err := rows.Scan(&id)
		if err != nil {
			return  err
		}
	}
	return  nil
}

func (contactManager ContactManger)createContactTable() error {
	msg := fmt.Sprintf(" CREATE TABLE contacts (id serial primary key ,firstname VARCHAR(200) NOT NULL ,lastname VARCHAR(200) NOT NULL , phone VARCHAR(200) NOT NULL UNIQUE , email VARCHAR(200) NOT NULL UNIQUE, position VARCHAR(200) NOT NULL );")
	_, err := contactManager.db.Exec(msg)
	if err != nil {
		return err
	}
	return nil

}
func (contactManager ContactManger)deleteContactTable() error {
	msg := fmt.Sprintf("DROP TABLE contacts;")
	_, err := contactManager.db.Exec(msg)
	if err != nil{
		return err
	}
	return nil
}